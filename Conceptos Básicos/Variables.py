""" En Python las variables le permite almacenar un valor asignándolo a un nombre, que se puede usar para referirse 
al valor más adelante en el programa"""  

x = 8
print(x)
#Salida = 8
print(2 * 8)
#Salida = 16

#En python las variables se pueden reasignar n veces, por ejemplo

x = 8
print(x)
#Salida = 8
x = 'Virtual Hero'
print(x) 
#Salida = Virtual Hero 

#La forma correcta de escribir las variables es con numeros, guiones bajos y letras

Esto_es_una_vaiable = 8

#Para eliminar una variable solo se debe de poner del, por ejemplo

x = 8
print(x)
#Salida = 8
del x


